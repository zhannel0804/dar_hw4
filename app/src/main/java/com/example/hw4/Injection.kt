package com.example.hw4

import android.content.Context
import com.example.hw4.entity.UserDao
import com.example.hw4.entity.UserRoomDatabase
import com.example.hw4.entity.ViewModelFactory

object Injection {

    private fun provideUserDataSource(context: Context): UserDao {
        val database = UserRoomDatabase.getInstance(context)
        return database.userDao()
    }

    fun provideViewModelFactory(context: Context): ViewModelFactory {
        val dataSource = provideUserDataSource(context)
        return ViewModelFactory(dataSource = dataSource)
    }
}